#include "epos4/epos4.hpp"
#include "ros/ros.h"
#include "std_msgs/String.h"

int main(int argc, char** argv) {
  ros::init(argc, argv, "epos4");
  ros::NodeHandle node_handler;

  epos4::Epos4 epos4;

  ros::spin();

  return 0;
}