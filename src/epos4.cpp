#include "epos4/epos4.hpp"

#include <Definitions.h>
#include <std_msgs/Int32.h>

#include <string>

namespace epos4 {

Epos4::Epos4()
    : node_handler("~"),
      clear_fault_service(node_handler.advertiseService(
          "clear_fault", &Epos4::clear_fault_callback, this)),
      set_enable_service(node_handler.advertiseService(
          "set_enable", &Epos4::set_enable_callback, this)),
      set_operation_mode(node_handler.advertiseService(
          "set_operation_mode", &Epos4::set_operation_mode_callback, this)),
      pub_timer(node_handler.createTimer(ros::Duration(0.1),
                                         &Epos4::pub_data_callback, this)) {
  XmlRpc::XmlRpcValue device_params;

  XmlRpc::XmlRpcValue default_device_params;
  default_device_params["device_name"] = "EPOS4";
  default_device_params["interface"] = "USB";
  default_device_params["portname"] = "USB0";
  default_device_params["protocol"] = "MAXON SERIAL V2";

  node_handler.param("device", device_params, default_device_params);

  device.device_name = std::string(device_params["device_name"]);
  device.interface = std::string(device_params["interface"]);
  device.port_name = std::string(device_params["portname"]);
  device.protocol = std::string(device_params["protocol"]);

  uint error_code = 0;

  // open device
  // TODO functionize
  device.key_handle = VCS_OpenDevice(
      (char*)device.device_name.c_str(), (char*)device.protocol.c_str(),
      (char*)device.interface.c_str(), (char*)device.port_name.c_str(),
      &error_code);
  if (device.key_handle > 0) {
    ROS_INFO("Successed to open Device | device:%s | port: %s",
             device.device_name.c_str(), device.port_name.c_str());
    uint baud_rate = 0;
    uint time_out = 0;
    if (VCS_GetProtocolStackSettings(device.key_handle, &baud_rate, &time_out,
                                     &error_code)) {
      time_out += 100;
      VCS_SetProtocolStackSettings(device.key_handle, baud_rate, time_out,
                                   &error_code);
    }
  } else {
    ROS_ERROR("Failed to open device! | device:%s | port: %s",
              device.device_name.c_str(), device.port_name.c_str());
  }

  // open sub device
  if (node_handler.hasParam("sub_device")) {
    // TODO functionize
    XmlRpc::XmlRpcValue sub_device_params;
    node_handler.getParam("sub_device", sub_device_params);

    sub_device.device_name = std::string(sub_device_params["device_name"]);
    sub_device.protocol = std::string(sub_device_params["protocol"]);

    sub_device.key_handle = VCS_OpenSubDevice(
        device.key_handle, (char*)sub_device.device_name.c_str(),
        (char*)sub_device.protocol.c_str(), &error_code);
    if (sub_device.key_handle > 0) {
      ROS_INFO("Successed to open sub device! | device:%s | protocol: %s",
               sub_device.device_name.c_str(), sub_device.protocol.c_str());
    } else {
      ROS_ERROR("Failed to open sub device! | device:%s | protocol: %s",
                sub_device.device_name.c_str(), sub_device.protocol.c_str());
    }
  }

  // motors
  XmlRpc::XmlRpcValue motors_param;
  XmlRpc::XmlRpcValue default_motors_param;
  default_motors_param[0]["id"] = 0;
  default_motors_param[0]["sub_device"] = false;

  node_handler.param("motors", motors_param, default_motors_param);
  for (int32_t i = 0; i < motors_param.size(); i++) {
    Motor motor;

    if (motors_param[i]["sub_device"]) {
      motor.key_handle = sub_device.key_handle;
    } else {
      motor.key_handle = device.key_handle;
    }

    const uint16_t id = static_cast<uint16_t>((int)motors_param[i]["id"]);

    uint8_t dimension;
    char notation;
    VCS_GetVelocityUnits(motor.key_handle, id, &dimension, &notation,
                         &error_code);
    motor.vel_notation = notation;

    const std::string motor_namespace = "motor" + std::to_string(id);
    // sub
    motor.move_with_velocity_sub = node_handler.subscribe(
        ros::names::append(motor_namespace, "move_with_velocity"), 1,
        &Epos4::Motor::move_with_velocity_callback, &motor);
    motor.move_to_position_sub = node_handler.subscribe(
        ros::names::append(motor_namespace, "move_to_position"), 1,
        &Epos4::Motor::move_to_position_callback, &motor);

    // pub
    motor.velocity_pub = node_handler.advertise<std_msgs::Float32>(
        ros::names::append(motor_namespace, "velocity"), 1);
    motor.position_pub = node_handler.advertise<std_msgs::Int32>(
        ros::names::append(motor_namespace, "position"), 1);
    motor.current_pub = node_handler.advertise<std_msgs::Int32>(
        ros::names::append(motor_namespace, "current"), 1);

    motors[id] = motor;

    ROS_INFO("motor | id: %d", id);

    // enable
    bool auto_enable;
    node_handler.param("auto_enable", auto_enable, true);
    if (auto_enable) {
      VCS_ClearFault(motor.key_handle, id, &error_code);
      VCS_SetEnableState(motor.key_handle, id, &error_code);
    }

    unsigned short state = 4;
    VCS_GetState(motor.key_handle, id, &state, &error_code);

    switch (state) {
      case 0:
        ROS_INFO("STATUS: Disabled");
        break;

      case 1:
        ROS_INFO("STATUS: Enabled");
        break;

      case 2:
        ROS_INFO("STATUS: quickstop");
        break;

      case 3:
        ROS_WARN("STATUS: FAULT");
        break;

      default:
        ROS_ERROR("motor not found");
        break;
    }
  }

  ROS_INFO("EPOS initialization completed!");
}

Epos4::~Epos4() {
  uint error_code;
  VCS_CloseAllSubDevices(device.key_handle, &error_code);
  ROS_INFO("All sub devices closed");
  VCS_CloseAllDevices(&error_code);
  ROS_INFO("All devices closed");
}

bool Epos4::clear_fault_callback(clear_faultRequest& req,
                                 clear_faultResponse& res) {
  const uint16_t& id = req.id;
  auto& result = res.successed;
  if (motors.find(id) == motors.end()) {
    ROS_ERROR("motor id:%2d not found", id);
    result.data = false;
    return result.data;
  }

  uint error_code;
  VCS_ClearFault(motors[id].key_handle, id, &error_code);
  ROS_INFO("cleared fault motor id:%2d", id);
  result.data = true;
  return result.data;
}

bool Epos4::set_enable_callback(set_enableRequest& req,
                                set_enableResponse& res) {
  const uint16_t& id = req.id;
  auto& result = res.successed;
  if (motors.find(id) == motors.end()) {
    ROS_ERROR("motor id:%2d not found", id);
    result.data = false;
    return result.data;
  }

  uint error_code;
  VCS_SetEnableState(motors[id].key_handle, id, &error_code);
  ROS_INFO("set enable motor id:%2d", id);
  result.data = true;
  return result.data;
}

bool Epos4::set_operation_mode_callback(set_operation_modeRequest& req,
                                        set_operation_modeResponse& res) {
  const uint16_t& id = req.id;
  auto& result = res.successed;
  if (motors.find(id) == motors.end()) {
    ROS_ERROR("motor id:%2d not found", id);
    result.data = false;
    return result.data;
  }

  uint error_code;
  VCS_SetOperationMode(motors[id].key_handle, req.mode, id, &error_code);
  ROS_INFO("set operation mode to:%2d", req.mode);
  result.data = true;
  return result.data;
}

void Epos4::Motor::move_with_velocity_callback(
    const std_msgs::Float32& velocity) {
  uint error_code;

  const long velocity_command = velocity.data * pow(10, -this->vel_notation);
  VCS_MoveWithVelocity(this->key_handle, this->id, velocity_command,
                       &error_code);
}

void Epos4::Motor::move_to_position_callback(const std_msgs::Int32& position) {
  uint error_code;

  VCS_MoveToPosition(this->key_handle, this->id, position.data, 1, 1,
                     &error_code);
}

void Epos4::pub_data_callback(const ros::TimerEvent& e) {
  for (const auto& motor : motors) {
    const auto& id = motor.first;
    const auto& keyHandle = motor.second.key_handle;
    uint error_code;

    int velocity;
    VCS_GetVelocityIs(keyHandle, id, &velocity, &error_code);
    std_msgs::Float32 velocity_rpm;
    velocity_rpm.data = velocity * pow(10.0f, motor.second.vel_notation);

    std_msgs::Int32 position;
    VCS_GetPositionIs(keyHandle, id, &position.data, &error_code);

    std_msgs::Int32 current;
    VCS_GetCurrentIsEx(keyHandle, id, &current.data, &error_code);

    motor.second.velocity_pub.publish(velocity_rpm);
    motor.second.position_pub.publish(position);
    motor.second.current_pub.publish(current);
  }
}

}  // namespace epos4