#pragma once

#include <ros/ros.h>
#include <std_msgs/Float32.h>
#include <std_msgs/Int32.h>

#include <map>

#include "epos4/clear_fault.h"
#include "epos4/set_enable.h"
#include "epos4/set_operation_mode.h"

namespace epos4 {

class Epos4 {
 public:
  Epos4();
  ~Epos4();

 private:
  /**
   * @brief デバイス情報
   *
   *
   */
  struct Device {
    std::string device_name;
    std::string protocol;
    std::string interface;
    std::string port_name;

    void* key_handle;
  };

  struct Motor {
    void* key_handle;
    uint16_t id;
    int8_t vel_notation;

    ros::Subscriber move_with_velocity_sub;
    ros::Subscriber move_to_position_sub;

    ros::Publisher velocity_pub;
    ros::Publisher position_pub;
    ros::Publisher current_pub;

    void move_with_velocity_callback(const std_msgs::Float32& velocity);
    void move_to_position_callback(const std_msgs::Int32& position);
  };

  bool clear_fault_callback(clear_faultRequest& req, clear_faultResponse& res);
  bool set_enable_callback(set_enableRequest& req, set_enableResponse& res);
  bool set_operation_mode_callback(set_operation_modeRequest& req,
                                   set_operation_modeResponse& res);

  void pub_data_callback(const ros::TimerEvent& e);

  ros::NodeHandle node_handler;
  ros::ServiceServer clear_fault_service;
  ros::ServiceServer set_enable_service;
  ros::ServiceServer set_operation_mode;
  ros::Timer pub_timer;

  Device device;
  Device sub_device;

  std::map<uint16_t, Motor> motors;
};

}  // namespace epos4